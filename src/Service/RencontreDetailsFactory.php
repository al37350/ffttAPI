<?php

namespace FFTTApi\Service;

use Accentuation\Accentuation;
use FFTTApi\Exception\NoFFTTResponseException;
use FFTTApi\FFTTApi;
use FFTTApi\Model\Rencontre\Joueur;
use FFTTApi\Model\Rencontre\Partie;
use FFTTApi\Model\Rencontre\RencontreDetails;

/**
 * Created by Antoine Lamirault.
 */
class RencontreDetailsFactory
{
    /**
     * @var FFTTApi
     */
    private $api;

    public function __construct(FFTTApi $api)
    {
        $this->api = $api;
    }

    public function createFromArray(array $array, string $clubEquipeA, string $clubEquipeB): RencontreDetails
    {

        $joueursA = [];
        $joueursB = [];
        foreach ($array['joueur'] as $joueur) {
            $joueursA[] = [$joueur['xja'] ?: '', $joueur['xca'] ?: ''];
            $joueursB[] = [$joueur['xjb'] ?: '', $joueur['xcb'] ?: ''];
        }
        $joueursAFormatted = $this->formatJoueurs($joueursA, $clubEquipeA);
        $joueursBFormatted = $this->formatJoueurs($joueursB, $clubEquipeB);

        $parties = $this->getParties($array['partie']);

        if (is_array($array['resultat']['resa'])) {
            $scores = $this->getScores($parties);
            $scoreA = $scores['scoreA'];
            $scoreB = $scores['scoreB'];
        } else {
            $scoreA = $array['resultat']['resa'] == "F0" ? 0 : $array['resultat']['resa'];
            $scoreB = $array['resultat']['resb'] == "F0" ? 0 : $array['resultat']['resb'];
        }


        $expected = $this->getExpectedPoints($parties, $joueursAFormatted, $joueursBFormatted);

        return new RencontreDetails(
            $array['resultat']['equa'],
            $array['resultat']['equb'],
            $scoreA,
            $scoreB,
            $joueursAFormatted,
            $joueursBFormatted,
            $parties,
            $expected['expectedA'],
            $expected['expectedB']
        );
    }

    /**
     * @param Partie[] $parties
     * @param array<string, Joueur> $joueursAFormatted
     * @param array<string, Joueur> $joueursBFormatted
     * @return array{expectedA: float, expectedB: float}
     */
    private function getExpectedPoints(array $parties, array $joueursAFormatted, array $joueursBFormatted): array
    {
        $expectedA = 0;
        $expectedB = 0;

        foreach ($parties as $partie) {
            $adversaireA = $partie->getAdversaireA();
            $adversaireB = $partie->getAdversaireB();

            if (isset($joueursAFormatted[$adversaireA])) {
                $joueurA = $joueursAFormatted[$adversaireA];
                $joueurAPoints = $joueurA->getPoints();
            } else {
                $joueurAPoints = 'NONE';
            }

            if (isset($joueursBFormatted[$adversaireB])) {
                $joueurB = $joueursBFormatted[$adversaireB];
                $joueurBPoints = $joueurB->getPoints();
            } else {
                $joueurBPoints = 'NONE';
            }

            if ($joueurAPoints === $joueurBPoints) {
                $expectedA += 0.5;
                $expectedB += 0.5;
            } elseif ($joueurAPoints > $joueurBPoints) {
                $expectedA += 1;
            } else {
                $expectedB += 1;
            }
        }

        return [
            'expectedA' => $expectedA,
            'expectedB' => $expectedB,
        ];
    }

    /**
     * @param Partie[] $parties
     * @return array{scoreA: int, scoreB: int}
     */
    private function getScores(array $parties): array
    {
        $scoreA = 0;
        $scoreB = 0;

        foreach ($parties as $partie) {
            $scoreA += $partie->getScoreA();
            $scoreB += $partie->getScoreB();
        }

        return [
            'scoreA' => $scoreA,
            'scoreB' => $scoreB,
        ];
    }

    /**
     * @param array $data
     * @param string $playerClubId
     * @return array<string, Joueur>
     */
    private function formatJoueurs(array $data, string $playerClubId): array
    {
        $joueursClub = $this->api->getJoueursByClub($playerClubId);

        $joueurs = [];
        foreach ($data as $joueurData) {
            $nomPrenom = $joueurData[0];
            [$nom, $prenom] = Utils::returnNomPrenom($nomPrenom);
            $joueurs[$nomPrenom] = $this->formatJoueur($prenom, $nom, $joueurData[1], $joueursClub);
        }
        return $joueurs;
    }

    /**
     * @param string $prenom
     * @param string $nom
     * @param string $points
     * @param array $joueursClub
     * @return Joueur
     */
    private function formatJoueur(string $prenom, string $nom, string $points, array $joueursClub): Joueur
    {
        if ($nom === "" && $prenom === "Absent") {
            return new Joueur($nom, $prenom, "", null, null);
        }

        try {
            foreach ($joueursClub as $joueurClub) {
                if ($joueurClub->getNom() === Accentuation::remove($nom) && $joueurClub->getPrenom() === $prenom) {

                    $return = preg_match('/^(N.[0-9]*- ){0,1}(?<sexe>[A-Z]{1}) (?<points>[0-9]+)pts$/', $points, $result);

                    if ($return === false) {
                        throw new \RuntimeException(
                            sprintf(
                                "Not able to extract sexe and points in '%s'",
                                $points
                            )
                        );
                    }
                    $sexe = $result['sexe'];
                    $playerPoints = $result['points'];

                    return new Joueur(
                        $joueurClub->getNom(),
                        $joueurClub->getPrenom(),
                        $joueurClub->getLicence(),
                        $playerPoints,
                        $sexe
                    );
                }
            }

        } catch (NoFFTTResponseException $e) {
        }

        return new Joueur($nom, $prenom, "", null, null);
    }

    /**
     * @param array $data
     * @return Partie[]
     */
    private function getParties(array $data): array
    {
        $parties = [];
        foreach ($data as $partieData) {
            $setDetails = explode(" ", $partieData['detail']);

            $parties[] = new Partie(
                $partieData['ja'] === [] ? 'Absent Absent' : $partieData['ja'],
                $partieData['jb'] === [] ? 'Absent Absent' : $partieData['jb'],
                $partieData['scorea'] === '-' ? 0 : intval($partieData['scorea']),
                $partieData['scoreb'] === '-' ? 0 : intval($partieData['scoreb']),
                $setDetails
            );
        }
        return $parties;
    }

}