<?php
/**
 * Created by Antoine Lamirault.
 */

namespace FFTTApi;

use Accentuation\Accentuation;
use FFTTApi\Exception\ClubNotFoundException;
use FFTTApi\Exception\InvalidLienRencontre;
use FFTTApi\Exception\JoueurNotFound;
use FFTTApi\Model\Actualite;
use FFTTApi\Model\Classement;
use FFTTApi\Model\ClubDetails;
use FFTTApi\Model\Equipe;
use FFTTApi\Model\EquipePoule;
use FFTTApi\Model\Historique;
use FFTTApi\Model\Joueur;
use FFTTApi\Model\JoueurDetails;
use FFTTApi\Model\Organisme;
use FFTTApi\Model\Partie;
use FFTTApi\Model\Club;
use FFTTApi\Model\VirtualPoints;
use FFTTApi\Model\UnvalidatedPartie;
use FFTTApi\Exception\InvalidCredidentials;
use FFTTApi\Exception\NoFFTTResponseException;
use FFTTApi\Model\Rencontre\Rencontre;
use FFTTApi\Model\Rencontre\RencontreDetails;
use FFTTApi\Service\ClubFactory;
use FFTTApi\Service\PointCalculator;
use FFTTApi\Service\RencontreDetailsFactory;
use FFTTApi\Service\Utils;
use GuzzleHttp\Exception\ClientException;
use DateTime;

class FFTTApi
{
    private $id;
    private $password;
    private $apiRequest;

    // Jour du début du mois virtuel
    const JOURDEBUTMOISVIRTUEL = 5;

    public function __construct(string $id, string $password)
    {
        $this->id = $id;
        $this->password = md5($password);
        $this->apiRequest = new ApiRequest($this->password, $this->id);
    }

    public function initialize()
    {
        $time = round(microtime(true) * 1000);
        $timeCrypted = hash_hmac("sha1", $time, $this->password);
        $uri = 'https://apiv2.fftt.com/mobile/pxml/xml_initialisation.php?serie=' . $this->id
            . '&tm=' . $time
            . '&tmc=' . $timeCrypted
            . '&id=' . $this->id;

        try{
            $response = $this->apiRequest->send($uri);
        }
        catch (ClientException $clientException){
            if($clientException->getResponse()->getStatusCode() === 401){
                throw new InvalidCredidentials();
            }
            throw $clientException;
        }

        return $response;
    }

    /**
     * @param string $type
     * @return Organisme[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getOrganismes(string $type = "Z"): array
    {
        if (!in_array($type, ['Z', 'L', 'D'])) {
            $type = 'L';
        }

        $organismes = $this->apiRequest->get('xml_organisme', [
            'type' => $type,
        ])["organisme"];

        $result = [];
        foreach ($organismes as $organisme) {
            $result[] = new Organisme(
                $organisme["libelle"],
                $organisme["id"],
                $organisme["code"],
                $organisme["idPere"]
            );
        }

        return $result;
    }

    /**
     * @param int $departementId
     * @return Club[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClubsByDepartement(int $departementId): array
    {

        $data = $this->apiRequest->get('xml_club_dep2', [
            'dep' => $departementId,
        ])['club'];

        $clubFactory = new ClubFactory();
        return $clubFactory->createFromArray($data);
    }

    /**
     * @param string $name
     * @return Club[]
     */
    public function getClubsByName(string $name)
    {
        try {
            $data = $this->apiRequest->get('xml_club_b', [
                'ville' => $name,
            ])['club'];

            $data = $this->wrappedArrayIfUnique($data);

            $clubFactory = new ClubFactory();
            return $clubFactory->createFromArray($data);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param string $clubId
     * @return ClubDetails
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClubDetails(string $clubId): ClubDetails
    {
        $clubData = $this->apiRequest->get('xml_club_detail', [
            'club' => $clubId,
        ])['club'];
        if (empty($clubData['numero'])) {
            throw new ClubNotFoundException($clubId);
        }
        return new ClubDetails(
            intval($clubData['numero']),
            $clubData['nom'],
            is_array($clubData['nomsalle']) ? null : $clubData['nomsalle'],
            is_array($clubData['adressesalle1']) ? null : $clubData['adressesalle1'],
            is_array($clubData['adressesalle2']) ? null : $clubData['adressesalle2'],
            is_array($clubData['adressesalle3']) ? null : $clubData['adressesalle3'],
            is_array($clubData['codepsalle']) ? null : $clubData['codepsalle'],
            is_array($clubData['villesalle']) ? null : $clubData['villesalle'],
            is_array($clubData['web']) ? null : $clubData['web'],
            is_array($clubData['nomcor']) ? null : $clubData['nomcor'],
            is_array($clubData['prenomcor']) ? null : $clubData['prenomcor'],
            is_array($clubData['mailcor']) ? null : $clubData['mailcor'],
            is_array($clubData['telcor']) ? null : $clubData['telcor'],
            is_array($clubData['latitude']) ? null : floatval($clubData['latitude']),
            is_array($clubData['longitude']) ? null : floatval($clubData['longitude'])
        );
    }

    /**
     * @param string $clubId
     * @return Joueur[]
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     */
    public function getJoueursByClub(string $clubId): array
    {
        try {
            $arrayJoueurs = $this->apiRequest->get('xml_liste_joueur_o', [
                    'club' => $clubId,
                ]
            );
        } catch (NoFFTTResponseException $e) {
            throw new ClubNotFoundException($clubId);
        }

        $result = [];

        foreach ($arrayJoueurs['joueur'] as $joueur) {
            $realJoueur = new Joueur(
                $joueur['licence'],
                $joueur['nclub'],
                $joueur['club'],
                $joueur['nom'],
                $joueur['prenom'],
                $joueur['points']);
            $result[] = $realJoueur;
        }
        return $result;
    }


    /**
     * @param string $nom
     * @param string $prenom
     * @return Joueur[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getJoueursByNom(string $nom, string $prenom = ""): array
    {
        $arrayJoueurs = $this->apiRequest->get('xml_liste_joueur', [
                'nom' => addslashes(Accentuation::remove($nom)),
                'prenom' => addslashes(Accentuation::remove($prenom)),
            ]
        )['joueur'];

        $arrayJoueurs = $this->wrappedArrayIfUnique($arrayJoueurs);

        $result = [];

        foreach ($arrayJoueurs as $joueur) {
            $realJoueur = new Joueur(
                $joueur['licence'],
                $joueur['nclub'],
                $joueur['club'],
                $joueur['nom'],
                $joueur['prenom'],
                $joueur['clast']);
            $result[] = $realJoueur;
        }
        return $result;
    }

    /**
     * @param string $licenceId
     * @return JoueurDetails
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getJoueurDetailsByLicence(string $licenceId): JoueurDetails
    {
        try {
            $data = $this->apiRequest->get('xml_licence_b', [
                    'licence' => $licenceId,
                ]
            )['licence'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceId);
        }

        $joueurDetails = new JoueurDetails(
            $licenceId,
            $data['nom'],
            $data['prenom'],
            $data['numclub'],
            $data['nomclub'],
            $data['sexe'] === 'M' ? true : false,
            $data['cat'],
            floatval($data['initm'] ?? floatval($data['point'])),
            floatval($data['point']),
            floatval($data['pointm'] ?? floatval($data['point'])),
            floatval($data['apointm'] ?? floatval($data['point']))
        );
        return $joueurDetails;
    }

    /**
     * @param string $licenceId
     * @return Classement
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getClassementJoueurByLicence(string $licenceId): Classement
    {
        try {
            $joueurDetails = $this->apiRequest->get('xml_joueur', [
                'licence' => $licenceId,
            ])['joueur'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceId);
        }

        $classement = new Classement(
            new \DateTime(),
            $joueurDetails['point'],
            $joueurDetails['apoint'],
            intval($joueurDetails['clast']),
            intval($joueurDetails['clnat']),
            intval($joueurDetails['rangreg']),
            intval($joueurDetails['rangdep']),
            intval($joueurDetails['valcla']),
            intval($joueurDetails['valinit'])
        );
        return $classement;
    }

    /**
     * @param string $licenceId
     * @return Historique[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getHistoriqueJoueurByLicence(string $licenceId): array
    {
        try {
            $classements = $this->apiRequest->get('xml_histo_classement', [
                'numlic' => $licenceId,
            ])['histo'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceId);
        }
        $result = [];
        $classements = $this->wrappedArrayIfUnique($classements);

        foreach ($classements as $classement) {
            $explode = explode(' ', $classement['saison']);

            $historique = new Historique($explode[1], $explode[3], intval($classement['phase']), intval($classement['point']));
            $result[] = $historique;
        }

        return $result;
    }

    /**
     * @param string $joueurId
     * @return Partie[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     */
    public function getPartiesJoueurByLicence(string $joueurId): array
    {

        try {
            $parties = $this->apiRequest->get('xml_partie_mysql', [
                'licence' => $joueurId,
            ])['partie'];
            $parties = $this->wrappedArrayIfUnique($parties);
        } catch (NoFFTTResponseException $e) {
            $parties = [];
        }
        $res = [];

        foreach ($parties as $partie) {
            list($nom, $prenom) = Utils::returnNomPrenom($partie['advnompre']);
            $realPartie = new Partie(
                $partie["vd"] === "V" ? true : false,
                intval($partie['numjourn']),
                \DateTime::createFromFormat('d/m/Y', $partie['date']),
                floatval($partie['pointres']),
                floatval($partie['coefchamp']),
                $partie['advlic'],
                $partie['advsexe'] === 'M' ? true : false,
                $nom,
                $prenom,
                intval($partie['advclaof'])
            );
            $res[] = $realPartie;
        }
        return $res;
    }

    /**
     * Détermine si la date d'une rencontre passée en paramètre correspond à la saison en cours
     * @param Datetime $dateRencontre
     * @return bool
     */
    public function isInCurrentSaison(Datetime $dateRencontre): bool {
        $today = new DateTime();

        $actualDay = $today->format('j');
        $actualMonth = $today->format('n');
        $actualYear = $today->format('Y');

        $dateDebutSaison = new DateTime($actualYear + ($actualMonth >= 7 && $actualDay >= 1 ? 0 : -1 ) . '-07-01');
        $dateFinSaison = new DateTime($actualYear + ($actualMonth >= 7 && $actualDay >= 1 ? 1 : 0 ) . '-07-01');

        return $dateRencontre >= $dateDebutSaison && $dateRencontre <= $dateFinSaison;
    }

    /**
     * Détermine si la date d'une rencontre passée en paramètre correspond au mois virtuel en cours, calculée du 1er [mois] inclus au 4 [mois+1] inclus
     * En d'autres termes, les rencontres virtuelles sont prises à partir du 1er du mois précédent jusqu'au 4 du mois en cours
     * Exemple : si nous sommes le 6 Octobre, nous prenons les rencontre du 1er Septembre à aujourd'hui.
     * Exemple : si nous sommes le 15 Octobre, nous prenons les rencontre du 1er Octobre (les points virtuels sont connus à partir du 5 et les rencontres sont comptabilisées du 1er au 31 du mois) à aujourd'hui.
     * @param Datetime $dateRencontre
     * @return bool
     */
    public function isInCurrentVirtualMonth(Datetime $dateRencontre): bool {
        $today = new DateTime();

        $actualMonth = $today->format('n');
        $actualYear = $today->format('Y');

        if ($today->format('j') > self::JOURDEBUTMOISVIRTUEL - 1){
            $debutMoisVirtuel = $actualMonth;
        } else {
            if ($actualMonth > 1) {
                $debutMoisVirtuel = $actualMonth - 1;
            }
            else {
                $debutMoisVirtuel = 12;
                $actualYear -= 1;
            }
        }

        $dateDebutMoisVirtuel = new DateTime($actualYear . '-' . $debutMoisVirtuel . '-01');
        $dateFinMoisVirtuel = new DateTime();

        return $dateRencontre >= $dateDebutMoisVirtuel && $dateRencontre <= $dateFinMoisVirtuel;
    }

    /**
     * @param string $joueurId
     * @return UnvalidatedPartie[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     */
    public function getUnvalidatedPartiesJoueurByLicence(string $joueurId): array
    {
        $validatedParties = $this->getPartiesJoueurByLicence($joueurId);

        try {
            $allParties = $this->apiRequest->get('xml_partie', [
                    'numlic' => $joueurId,
                ])["partie"] ?? [];
        } catch (NoFFTTResponseException $e) {
            $allParties = [];
        }

        $result = [];
        foreach ($allParties as $partie) {
            if ($partie["victoire"] !== "V" || $partie["forfait"] !== "1") {
                list($nom, $prenom) = Utils::returnNomPrenom($partie['nom']);
                $found = count(array_filter($validatedParties, function($validatedPartie) use ($partie, $nom, $prenom) {
                    return $partie["date"] === $validatedPartie->getDate()->format("d/m/Y")
                        and Utils::removeAccentLowerCaseRegex($nom) === Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversaireNom())
                        and (
                            preg_match('/' . Utils::removeAccentLowerCaseRegex($prenom) . '.*/', Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversairePrenom())) or
                            str_contains(Utils::removeAccentLowerCaseRegex($prenom), Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversairePrenom()))
                        );
                }));

                if (!$found and $prenom != "Absent Absent"
                    and $this->isInCurrentSaison(\DateTime::createFromFormat('d/m/Y', $partie['date']))
                    and $this->isInCurrentVirtualMonth(\DateTime::createFromFormat('d/m/Y', $partie['date']))) {
                    $result[] = new UnvalidatedPartie(
                        $partie["epreuve"],
                        $partie["idpartie"],
                        floatval($partie["coefchamp"]),
                        $partie["victoire"] === "V",
                        $partie["forfait"] === "1",
                        \DateTime::createFromFormat('d/m/Y', $partie['date']),
                        $nom,
                        $prenom,
                        Utils::formatPoints($partie["classement"])
                    );
                }
            }
        }
        return $result;
    }

    /**
     * @param string $joueurId
     * @return VirtualPoints Objet contenant les points gagnés/perdus et le classement virtuel du joueur
     */
    public function getJoueurVirtualPoints(string $joueurId): VirtualPoints
    {
        $pointCalculator = new PointCalculator();

        try {
            $classement = $this->getClassementJoueurByLicence($joueurId);
            $virtualMonthlyPointsWon = 0.0;
            $virtualMonthlyPoints = 0.0;
            $latestMonth = null;
            $monthPoints = round($classement->getPoints(), 1);

            $unvalidatedParties = $this->getUnvalidatedPartiesJoueurByLicence($joueurId);

            usort($unvalidatedParties, function (UnvalidatedPartie $a, UnvalidatedPartie $b) {
                return $a->getDate() >= $b->getDate();
            });

            foreach ($unvalidatedParties as $unvalidatedParty) {
                if (!$latestMonth) {
                    $latestMonth = $unvalidatedParty->getDate()->format("m");
                } else {
                    if ($latestMonth != $unvalidatedParty->getDate()->format("m") && $unvalidatedParty->getDate()->format("j") > self::JOURDEBUTMOISVIRTUEL - 1) {
                        $monthPoints = round($classement->getPoints() + $virtualMonthlyPointsWon, 1);
                        $latestMonth = $unvalidatedParty->getDate()->format("m");
                    }
                }

                $coeff = $unvalidatedParty->getCoefficientChampionnat();

                if (!$unvalidatedParty->isForfait()) {
                    $adversairePoints = $unvalidatedParty->getAdversaireClassement();

                    /**
                     * TODO Refactoring in method
                     */

                    try {
                        $availableJoueurs = $this->getJoueursByNom($unvalidatedParty->getAdversaireNom(), $unvalidatedParty->getAdversairePrenom());
                        foreach ($availableJoueurs as $availableJoueur) {
                            if (round(($unvalidatedParty->getAdversaireClassement() / 100)) == $availableJoueur->getPoints()) {
                                $classementJoueur = $this->getClassementJoueurByLicence($availableJoueur->getLicence());
                                $adversairePoints = round($classementJoueur->getPoints(), 1);
                                break;
                            }
                        }

                    } catch (NoFFTTResponseException $e) {
                        $adversairePoints = $unvalidatedParty->getAdversaireClassement();
                    }catch (InvalidURIParametersException $e) {
                        $adversairePoints = $unvalidatedParty->getAdversaireClassement();
                    }

                    $points = $unvalidatedParty->isVictoire()
                        ? $pointCalculator->getPointVictory($monthPoints, floatval($adversairePoints))
                        : $pointCalculator->getPointDefeat($monthPoints, floatval($adversairePoints));
                    $virtualMonthlyPointsWon += $points * $coeff;
                }
            }

            $virtualMonthlyPoints = $monthPoints + $virtualMonthlyPointsWon;
            return new VirtualPoints(
                $virtualMonthlyPointsWon,
                $virtualMonthlyPoints,
                $virtualMonthlyPoints - $classement->getPointsInitials()
            );
        } catch (JoueurNotFound $e) {
            return new VirtualPoints(0.0, 0.0, 0.0);
        }
    }

    /**
     * @param string $joueurId
     * @return float points mensuels gagnés ou perdus en fonction des points mensuels de l'adversaire
     */
    public function getVirtualPoints(string $joueurId) : float {
        return $this->getJoueurVirtualPoints($joueurId)->getPointsWon();
    }

    /**
     * @param string $clubId
     * @param string|null $type
     * @return Equipe[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getEquipesByClub(string $clubId, string $type = null)
    {
        $params = [
            'numclu' => $clubId,
        ];
        if ($type) {
            $params['type'] = $type;
        }

        $data = $this->apiRequest->get('xml_equipe', $params
        )['equipe'];
        $data = $this->wrappedArrayIfUnique($data);


        $result = [];
        foreach ($data as $dataEquipe) {
            $result[] = new Equipe(
                $dataEquipe['libequipe'],
                $dataEquipe['libdivision'],
                $dataEquipe['liendivision']
            );
        }
        return $result;
    }

    /**
     * @param string $lienDivision
     * @return EquipePoule[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClassementPouleByLienDivision(string $lienDivision): array
    {
        $data = $this->apiRequest->get('xml_result_equ', ["action" => "classement"], $lienDivision)['classement'];
        $result = [];
        $lastClassment = 0;
        foreach ($data as $equipePouleData) {

            if (!is_string($equipePouleData['equipe'])) {
                break;
            }

            $result[] = new EquipePoule(
                $equipePouleData['clt'] === '-' ? $lastClassment : intval($equipePouleData['clt']),
                $equipePouleData['equipe'],
                intval($equipePouleData['joue']),
                intval($equipePouleData['pts']),
                $equipePouleData['numero'],
                intval($equipePouleData['totvic']),
                intval($equipePouleData['totdef']),
                intval($equipePouleData['idequipe']),
                $equipePouleData['idclub']
            );
            $lastClassment = $equipePouleData['clt'] == "-" ? $lastClassment : intval($equipePouleData['clt']);
        }
        return $result;
    }

    /**
     * @param string $lienDivision
     * @return Rencontre[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getRencontrePouleByLienDivision(string $lienDivision): array
    {
        $data = $this->apiRequest->get('xml_result_equ', [], $lienDivision)['tour'];


        $result = [];
        foreach ($data as $dataRencontre) {
            $equipeA = $dataRencontre['equa'];
            $equipeB = $dataRencontre['equb'];

            $result[] = new Rencontre(
                $dataRencontre['libelle'],
                is_array($equipeA) ? '': $equipeA,
                is_array($equipeB) ? '': $equipeB,
                intval($dataRencontre['scorea']),
                intval($dataRencontre['scoreb']),
                $dataRencontre['lien'],
                \DateTime::createFromFormat('d/m/Y', $dataRencontre['dateprevue']),
                empty($dataRencontre['datereelle']) ? null : \DateTime::createFromFormat('d/m/Y', $dataRencontre['datereelle'])
            );
        }
        return $result;
    }


    /**
     * @param Equipe $equipe
     * @return Rencontre[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getProchainesRencontresEquipe(Equipe $equipe): array
    {
        $nomEquipe = Utils::extractNomEquipe($equipe);
        $rencontres = $this->getRencontrePouleByLienDivision($equipe->getLienDivision());

        $prochainesRencontres = [];
        foreach ($rencontres as $rencontre) {
            if ($rencontre->getDateReelle() === null && $rencontre->getNomEquipeA() === $nomEquipe || $rencontre->getNomEquipeB() === $nomEquipe) {
                $prochainesRencontres[] = $rencontre;
            }
        }
        return $prochainesRencontres;
    }

    /**
     * @param Equipe $equipe
     * @return ClubDetails|null
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClubEquipe(Equipe $equipe): ?ClubDetails
    {
        $nomEquipe = Utils::extractClub($equipe);
        $club = $this->getClubsByName($nomEquipe);

        if(count($club) === 1){
            return $this->getClubDetails($club[0]->getNumero());
        }

        return null;
    }

    /**
     * @param string $lienRencontre
     * @param string $clubEquipeA
     * @param string $clubEquipeB
     * @return RencontreDetails
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws InvalidLienRencontre
     * @throws NoFFTTResponseException
     */
    public function getDetailsRencontreByLien(string $lienRencontre, string $clubEquipeA = "", string $clubEquipeB = ""): RencontreDetails
    {
        $data = $this->apiRequest->get('xml_chp_renc', [], $lienRencontre);
        if (!(isset($data['resultat']) && isset($data['joueur']) && isset($data['partie']))) {
            throw new InvalidLienRencontre($lienRencontre);
        }
        $factory = new RencontreDetailsFactory($this);
        return $factory->createFromArray($data, $clubEquipeA, $clubEquipeB);
    }

    /**
     * @return Actualite[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getActualites(): array
    {
        $data = $this->apiRequest->get('xml_new_actu')['news'];
        $data = $this->wrappedArrayIfUnique($data);

        $result = [];
        foreach ($data as $dataActualite) {
            $result[] = new Actualite(
                \DateTime::createFromFormat('Y-m-d', $dataActualite["date"]),
                $dataActualite['titre'],
                $dataActualite['description'],
                $dataActualite['url'],
                $dataActualite['photo'],
                $dataActualite['categorie']
            );
        }
        return $result;
    }

    private function wrappedArrayIfUnique($array): array
    {
        if (count($array) == count($array, COUNT_RECURSIVE)) {
            return [$array];
        }
        return $array;
    }
}
