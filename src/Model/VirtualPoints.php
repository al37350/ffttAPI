<?php
/**
 * Created by Stephen Sakovitch.
 */

namespace FFTTApi\Model;


class VirtualPoints
{
    private $seasonlyPointsWon;
    private $monthlyPointsWon;
    private $virtualPoints;

    public function __construct(float $monthlyPointsWon, float $virtualPoints, float $seasonlyPointsWon)
    {
        $this->monthlyPointsWon = $monthlyPointsWon;
        $this->virtualPoints = $virtualPoints;
        $this->seasonlyPointsWon = $seasonlyPointsWon;
    }

    public function getSeasonlyPointsWon(): float
    {
        return $this->seasonlyPointsWon;
    }

    public function getPointsWon(): float
    {
        return $this->monthlyPointsWon;
    }

    public function getVirtualPoints(): float
    {
        return $this->virtualPoints;
    }
}